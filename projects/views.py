from django.shortcuts import redirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    # #context_userproject = Project.objects.filter(members=self.request.user)
    #    context["userproject"] = context_userproject
    #     return context


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/new.html"
    fields = ["name", "description", "members"]

    def form_valid(self, form):
        returnedQuerySet = form.cleaned_data.get("members")
        name = form.cleaned_data.get("name")
        description = form.cleaned_data.get("description")
        newproject = Project.objects.create(name=name, description=description)
        for member in returnedQuerySet.iterator():
            newproject.members.add(member)
            return redirect("show_project", pk=newproject.id)
