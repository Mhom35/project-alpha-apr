from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def form_valid(self, form):
        form.instance.tasks = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        # filter out to see only the assignee potss
        return Task.objects.filter(assignee=self.request.user)


class CompleteTaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["is_completed"]

    def get_success_url(self):
        return reverse_lazy("show_my_tasks")
