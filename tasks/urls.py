from django.urls import path
from tasks.views import TaskCreateView, TaskListView, CompleteTaskUpdateView

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path(
        "<int:pk>/complete/",
        CompleteTaskUpdateView.as_view(),
        name="complete_task",
    ),
]
