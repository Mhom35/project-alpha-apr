from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

# Create your views here.


def SignUp(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)  # make instance of UserCform
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create(
                username=username,
                password=password,
            )
            user.save()  # save our new instance of a user
            login(request, user)  # login our new user
            return redirect("home")  # redirect that user home after login
    else:
        form = UserCreationForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
